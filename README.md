# Onboarding Project Public
The **Onboarding Project** is a simple web app, which resembles an ePosters Submission system.  
It uses Angular for the fronted, Node.js for the backend which acts as a REST API and MySQL as a database.

## Description
  
This app has five screens  
1.  Home Page: Here the user is greeted with a welcoming message and receives instructions on how to submit his/her file. Pressing `Submit Poster` button to proceed to the next screen
2.  Unique Submission Code: In this screen the user is required to enter a Poster Submission Code which she has received by email
3.  Title & Authors: In this screen the user can view the information associated with submission code he entered on the previous step
4.  File Upload: This is where the user uploads her poster
5.  Done: End of the process where the user is informed if the process has been completed successfully
  

## Setup Requirements
- Node.js = 12.13.1 (LTS)
- NPM = 6.12.1
- Angular >= 8
- Angular CLI >= 8
- MySQL >= 5.6.28


## Installation
1. Install angular-cli globally `npm install -g @angular/cli`
2. Clone repository locally
3. Install dependencies in client & server directory separately `npm install`
4. Install MySQL, depending on your OS the easiest way is to use XAMPP/MAMP or other LAMP stack installer
5. Import rc2019_onboarding.sql in MySQL

## Run
- run client `ng serve --open` (--open flag to automatically open in a new tab in browser)
- run server `npm start`, server uses nodemon to track file changes and restart automatically
- make sure MySQL is running

#### See [Issues](https://gitlab.com/angelos_scigen/onboarding-project-public/issues) for the tasks required to complete this project