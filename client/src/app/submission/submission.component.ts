import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubmissionService } from './submission.service';

@Component({
  selector: 'app-submission',
  templateUrl: './submission.component.html',
  styleUrls: ['./submission.component.scss']
})
export class SubmissionComponent implements OnInit {

  submissionCodeFormGroup: FormGroup;
  uploadFileFormGroup: FormGroup;

  submission$;

  submissionCode;

  constructor(
    private formBuilder: FormBuilder,
    private submissionService: SubmissionService
  ) {

  }

  ngOnInit() {
    this.submissionCodeFormGroup = this.formBuilder.group({
      submissionCodeCtrl: ['', Validators.required] // add validation to make it sql injection safe
    });
    this.uploadFileFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  public getSubmission() {
    if (this.submissionCodeFormGroup.valid) {
      console.log(this.submissionCodeFormGroup.value); // better way to access form values
      this.submissionCode = this.submissionCodeFormGroup.value.submissionCodeCtrl;
      this.submission$ = this.submissionService.getSubmission(this.submissionCode);
    }
  }

  completeStep(step: number) {
    this.submissionService.completeStep(this.submissionCode, step).subscribe();
  }

}
