-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: rc2019_onboarding
-- ------------------------------------------------------
-- Server version	5.6.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `submissioncodes`
--

DROP TABLE IF EXISTS `submissioncodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submissioncodes` (
  `unique_identifier` varchar(45) NOT NULL DEFAULT '',
  `poster_code` varchar(45) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `abstract_title` longtext,
  `abstract_id` int(11) DEFAULT NULL,
  `poster_title` longtext,
  `step_1` tinyint(1) NOT NULL DEFAULT '0',
  `step_2` tinyint(1) NOT NULL DEFAULT '0',
  `step_3` tinyint(1) NOT NULL DEFAULT '0',
  `step_4` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`unique_identifier`),
  UNIQUE KEY `unique_identifier_UNIQUE` (`unique_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submissioncodes`
--

LOCK TABLES `submissioncodes` WRITE;
/*!40000 ALTER TABLE `submissioncodes` DISABLE KEYS */;
INSERT INTO `submissioncodes` VALUES ('rc2019.0010001','P01','John ','','Smith','john.psaros@scigentech.com',1,'Demo Poster',1,NULL,0,0,0,0),('rc2019.0020002','P02','Angelos','','Jones','angelos.ar@scigentech.com',2,'Demo Poster 2',2,NULL,0,0,0,0),('rc2019.0030003','P03','Sophie','','Rodrigez','natasa.chris@scigentech.com',3,'Demo Poster 3',3,NULL,0,0,0,0),('rc2019.0040004','P04','Ana',NULL,'Chris','anachris@foo.com',4,'Demo Poster 4',4,NULL,0,0,0,0);
/*!40000 ALTER TABLE `submissioncodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'rc2019_onboarding'
--

--
-- Dumping routines for database 'rc2019_onboarding'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-01 21:58:32
